package calculator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	Interpreter calculator;

	@Before
	public void makeCalulator() {
		calculator = new Interpreter();

	}

	@Test
	public void test1() {
		assertExpression("", null);
	}

	@Test
	public void test2() {
		assertExpression("1+1;", new IntValue(2));
		assertExpression("2*(1.5+4.5);", new FloatValue((float) 12));
		assertExpression("x = 1.5; 2*(x+4.5);", new FloatValue((float) 12));
		assertExpression("{x = 5.5;} {5>=x;}", FalseValue.getInstance());
	}

	@Test
	public void testIf() {
		assertExpression("{x = 5.5;} if(x>=5){x=0;} x+2;", new IntValue(2));
		assertExpression("{x = 5.5;} if(x<5){x=0;} x+2;", new FloatValue((float) 7.5));
	}

	@Test
	public void testWhile() {
		assertExpression("{x = 5.5;} while(x>=5){x = x-1;} x+2;", new FloatValue((float) 6.5));
		assertExpression("{x = 5.5;} while(x<5){x = x-1;} x+2;", new FloatValue((float) 7.5));
	}

	@Test
	public void testFor() {
		assertExpression("y = 1; for(x = 0; x < 5; x = x+1){y= y +1;} y+2;", new IntValue(8));
		assertExpression("y = 1; for(x = 0, z = 2; z < 5; x = x+1, z=z+4){y= y +1;} y+2;", new IntValue(4));
	}

	private void assertExpression(String program, Value answer) {
		Value result = calculator.calculate(program);
		assertEquals(result, answer);
	}

}
