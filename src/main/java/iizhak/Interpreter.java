package iizhak;

import java.io.*;

public class Interpreter {

	public void interpretate(String str) {

		try {
			Lexer lex = new Lexer();
			Token[] tokens = lex.tokenize(str);
			Compiler comp = new Compiler();
			State state = new State();
			FCodeWord bWord = comp.compile(tokens, state);
			bWord.call(state);
			int ans = state.getStack().top().toInt();
			System.out.print("answer: " + ans + "\n");
		} catch (EmptyStackException e) {
			System.out.print("invalid stack operation");
			return;
		} catch (InvalidSymbolException e) {
			System.out.print("invalid symbol");
			return;
		}
	}

	public static void help() {
		System.out.print(
				"Interpreter gets double numbers and arithmetic operator and returns double result of this operation.\n"
						+ "Input should be separated with spaces \"double double double operator operator double operator...\""
						+ "(8.5 5.2 + 5.4 5.6 + 4.0 * etc.) \n");

	}

	private String fileTostring(String fileName) {
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			String fileStrings;
			StringBuilder strBuilder = new StringBuilder();
			while ((fileStrings = br.readLine()) != null) {
				strBuilder = strBuilder.append(fileStrings + " ");
			}
			return strBuilder.toString();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}

		return null;
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			help();
			return;
		}
		String fileName = args[0];

		if (fileName.equals("--help")) {
			help();
			return;
		}

		Interpreter interpr = new Interpreter();
		String fileString = interpr.fileTostring(fileName);
		interpr.interpretate(fileString);
	}

}
