package iizhak;

import java.util.*;

public class Table {
	private ArrayList<FWord> list;
	private Dictionary<String, Integer> dict;

	public Table() {
		list = new ArrayList<FWord>();
		dict = new Dictionary<String, Integer>();
	}

	public int getIndex(String str) {
		if (dict.get(str) == null) {
			return -1;
		}
		return dict.get(str);
	}

	public int put(String str, FWord word) {
		int index = getIndex(str);
		if (index == -1) {
			list.add(word);
			index = list.size() - 1;
			dict.put(str, index);
		}
		return index;
	}
	
	public int set(String str, FWord word) {
		int index = getIndex(str);
		if (index == -1) {
			throw new IndexOutOfBoundsException();
		}
		list.set(index, word);
		return index;
	}

	public FWord getWord(int i) {
		if (i > list.size()) {
			return null;
		}
		return list.get(i);
	}

	public static void main(String[] args) {
		Table t = new Table();
		int i0 = t.put("+", new Add());
		int i1 = t.put("-", new Sub());
		int i2 = t.put("*", new Mul());
		int i3 = t.put("+", new Add());
		int i4 = t.put("/", new Div());
		System.out.println(i0 + ", " + i1 + ", " + i2 + ", " + i3 + ", " + i4);
		int i5 = t.getIndex("*");
		System.out.println(i5);
		FWord w0 = t.getWord(6);
		FWord w1 = t.getWord(3);
		System.out.println(w0+ ", " + w1);
	}
}
