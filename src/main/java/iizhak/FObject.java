package iizhak;

import java.util.*;

public class FObject {
	public int toInt() {
		throw new RuntimeException("invalid call");
	}
	
	public void call(State state) {
		throw new RuntimeException("invalid call");
	}
	
	public String toString() {
		throw new RuntimeException("invalid call");
	}
}

class FNum extends FObject {
	private int i;

	public FNum(int num) {
		this.i = num;
	}

	public int toInt() {
		return i;
	}
	
	public String toString() {
		return " " + i + " ";
	}
}

class FWord extends FObject {
}

class FCodeWord extends FWord {
	ArrayList<Integer> bCode;

	public FCodeWord(ArrayList<Integer> b) {
		this.bCode = b;
	}

	public ArrayList<Integer> getCode() {
		return bCode;
	}
	
	public void call(State state) {
		Stack stack = state.getStack();
		Library lib = state.getLib();
		int i = 0;
		while (i < bCode.size()) {
			int code = bCode.get(i);
			int command = Binary.unpackCom(code);
			if (command == Compiler.NUMBER) {
				int num = Binary.unpackArg(code);
				stack.push(new FNum(num));
			} else if (command == Compiler.CALL) {
				int index = Binary.unpackArg(code);
				FWord w = lib.getWord(index);
				w.call(state);
			} else if (command == Compiler.JUMP_IF_FALSE) {
				int index = Binary.unpackArg(code);
				int num = stack.pop().toInt();
				if (num == 0) {
					i = index;
					continue;
				}
			} else if (command == Compiler.JUMP) {
				int index = Binary.unpackArg(code);
				i = index;
				continue;
			} 
			
			i++;
		}
	}
}

abstract class FNativeWord extends FWord {
}
