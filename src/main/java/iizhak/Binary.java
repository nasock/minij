package iizhak;

public class Binary {
	
	static int COM_BITS = 6;
	static int ARG_BITS = 32 - COM_BITS;
	static int MAX_COM = ((1 <<COM_BITS) -1);
	static int MAX_ARG = ((1 <<ARG_BITS) -1);
	static int COM_MASK = (MAX_COM <<ARG_BITS);
	
	
	static public String intToBinString(int num) {
		StringBuilder strb = new StringBuilder();
		for (int i = 31; i >= 0; i--) {
			int j = num & (1 << i);
			if (j == 0) {
				strb.append("0");
			} else {
				strb.append("1");
			}
		}
		
		return strb.toString();
	}
	
	static public int pack(int comNum, int argNum) {
		if (comNum > MAX_COM || argNum > MAX_ARG) {
			throw new IndexOutOfBoundsException();
		}
		return (comNum << ARG_BITS) + (argNum);
	}
	
	static public int unpackCom(int num) {
		return (num & COM_MASK) >>>ARG_BITS;
	}
	
	static public int unpackArg(int num) {
		return num & MAX_ARG;
	}
	
	public static void main(String[] args) {
		int packed = Binary.pack(63, 67108863);
		int unpackedCom = Binary.unpackCom(packed);
		int unpackedArg = Binary.unpackArg(packed);

		System.out.println(Binary.MAX_COM);
		System.out.println(Binary.MAX_ARG);
		String str = Binary.intToBinString(packed);
		String str1 = Binary.intToBinString(unpackedCom);
		System.out.println(str);
		System.out.println(str1);

		System.out.println(unpackedCom + ", " + unpackedArg);
	}
	
}
