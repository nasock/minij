package iizhak;

import java.util.*;

public class Compiler {
	static final int NUMBER = 0;
	static final int CALL = 1;
	static final int JUMP_IF_FALSE = 2;
	static final int JUMP = 3;

	public FCodeWord compile(Token[] tokens, State state) {
		TokenStream tokenStream = new TokenStream(tokens);
		Library lib = state.getLib();
		FCodeWord bWord = _compile(tokenStream, lib);
		return bWord;
	}

	private FCodeWord _compile(TokenStream tokenStream, Library lib) {
		ArrayList<Integer> bCode = new ArrayList<Integer>();
		LinkedList<JumpInfo> jumpQueue = new LinkedList<JumpInfo>();
		
		while (tokenStream.hasNext()) {
			Token token = tokenStream.next();

			if (token.getType() == Token.TokenType.NUMBER) {
				int num = Integer.parseInt(token.getData());
				int binary = Binary.pack(NUMBER, num);
				bCode.add(binary);

			} else if (token.getType() == Token.TokenType.CALL) {
				String data = token.getData();
				int word = lib.getIndex(data);
				if (word == -1) {
					throw new RuntimeException("unknown word");
				}
				int binary = Binary.pack(CALL, word);
				bCode.add(binary);

			} else if (token.getType() == Token.TokenType.BEGIN) {
				int index = bCode.size();
				jumpQueue.push(new JumpInfo(index, JumpInfo.JumpType.BEGIN));
				
			} else if (token.getType() == Token.TokenType.WHILE) {
				int binary = Binary.pack(JUMP_IF_FALSE, 0);
				bCode.add(binary);
				int index = bCode.size() - 1;
				jumpQueue.push(new JumpInfo(index, JumpInfo.JumpType.WHILE));

			} else if (token.getType() == Token.TokenType.IF) {
				int binary = Binary.pack(JUMP_IF_FALSE, 0);
				bCode.add(binary);
				int index = bCode.size() - 1;
				jumpQueue.push(new JumpInfo(index, JumpInfo.JumpType.IF));

			} else if (token.getType() == Token.TokenType.ELSE) {
				int indexIf = jumpQueue.pop().index;
				int binaryElse = Binary.pack(JUMP, 0);
				bCode.add(binaryElse);
				int indexElse = bCode.size() - 1;
				int binaryIf = Binary.pack(JUMP_IF_FALSE, indexElse + 1);
				bCode.set(indexIf, binaryIf);
				jumpQueue.push(new JumpInfo(indexElse, JumpInfo.JumpType.ELSE));
				
			} else if (token.getType() == Token.TokenType.END) {
				JumpInfo jump = jumpQueue.pop();
				JumpInfo.JumpType type = jump.type;
				int indexJump = jump.index;
				if (type == JumpInfo.JumpType.ELSE) {
					int indexEnd = bCode.size();
					int binaryElse = Binary.pack(JUMP, indexEnd);
					bCode.set(indexJump, binaryElse);
				} else if (type == JumpInfo.JumpType.WHILE) {
					JumpInfo jumpBegin = jumpQueue.pop();
					int indexBegin = jumpBegin.index;
					int binaryEnd = Binary.pack(JUMP, indexBegin);
					bCode.add(binaryEnd);
					int indexEnd = bCode.size();
					int binaryWhile = Binary.pack(JUMP_IF_FALSE, indexEnd);
					bCode.set(indexJump, binaryWhile);
				} else if (type == JumpInfo.JumpType.IF){
					int indexEnd = bCode.size();
					int binaryIf = Binary.pack(JUMP_IF_FALSE, indexEnd);
					bCode.set(indexJump, binaryIf);
				}

			} else if (token.getType() == Token.TokenType.COLON) {
				token = tokenStream.next();
				String name = token.getData();
				lib.put(name, null);
				FCodeWord word = _compile(tokenStream, lib);
				lib.set(name, word);

			} else if (token.getType() == Token.TokenType.SEMICOLON) {
				return new FCodeWord(bCode);
			}
		}
		return new FCodeWord(bCode);
	}
}

class JumpInfo {
	int index;
	JumpType type;

	public JumpInfo(int ind, JumpType type) {
		this.index = ind;
		this.type = type;
	}

	public enum JumpType {
		BEGIN, WHILE, IF, ELSE
	}

}
