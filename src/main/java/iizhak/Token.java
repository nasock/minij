package iizhak;

public class Token {
	private TokenType type;
	private String data;

	public Token(TokenType type, String data) {
		this.type = type;
		this.data = data;
	}

	public enum TokenType {
		NUMBER, CALL, COLON, SEMICOLON, BEGIN, WHILE, IF, ELSE, END
	}

	public TokenType getType() {
		return type;
	}

	public String getData() {
		return data;
	}

}
