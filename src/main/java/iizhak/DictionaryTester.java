package iizhak;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class DictionaryTester {
	Dictionary<String, Integer> dic;
	
	@Before
	public void setUp() {
		dic = new Dictionary<String, Integer>();
		dic.put("s1", 1);
		dic.put("s2", 2);
		dic.put("s3", 3);
		dic.put("s7", 7);
		dic.put("s8", 8);
		dic.put("s9", 9);
	}
	
	@Test
	public void testPut() {
		dic.put("s4", 4);
		dic.put("s5", 5);
		dic.put("s6", 6);
		dic.put("s3", 333);
		assertEquals(9, dic.size());
		assertEquals((Integer)333, dic.get("s3"));
		
		try {
			dic.put("s11", null);
			fail("invalid element");
		} catch (NullPointerException e) {

		}
		
		try {
			dic.put(null, 4);
			fail("invalid element");
		} catch (NullPointerException e) {

		}
	}
	
	@Test
	public void testGet() {
		assertEquals((Integer)9, dic.get("s9"));
		assertEquals(null, dic.get("s75"));
		
		try {
			dic.get(null);
			fail("invalid element");
		} catch (NullPointerException e) {

		}
	}
	
	@Test
	public void testDelete() {
		assertEquals((Integer)7, dic.delete("s7"));
		assertEquals(null, dic.delete("s75"));
		
		try {
			dic.delete(null);
			fail("invalid element");
		} catch (NullPointerException e) {

		}
	}
}
