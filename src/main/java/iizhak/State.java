package iizhak;

public class State {
	private Library lib;
	private Stack stack;
	
	public State() {
		this.lib = new Library();
		this.stack = new Stack();
	}
	
	public Library getLib() {
		return lib;
	}
	
	public Stack getStack() {
		return stack;
	}
}
