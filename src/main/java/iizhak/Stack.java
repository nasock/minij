package iizhak;

import java.util.*;

public class Stack {
	private FObject[] stack;
	private int currentIndex;
	
	public Stack() {
		this.stack = new FObject[2];
		currentIndex = 0;
	}
	
	public Stack(int size) {
		this.stack = new FObject[size];
		currentIndex = 0;
	}
	
	public void push(FObject num) {
		if (currentIndex == stack.length) {
			FObject[] newStack = Arrays.copyOf(stack, stack.length*2);
			stack = newStack;
		}
		stack[currentIndex] = num;
		currentIndex++;
		
	}
	
	public FObject pop() {
		if (stack.length == 0) {
			 throw new EmptyStackException();
		}
		currentIndex--;
		return stack[currentIndex];
	}
	
	public FObject top() {
		if (stack.length == 0) {
			 throw new EmptyStackException();
		}
		return stack[currentIndex-1];
	}
	
	public FObject get(int index) {
		if (index >= currentIndex || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		return stack[index];
	}
	
	public FObject delete(int index) {
		FObject num = get(index);
		for (int i = index; i < currentIndex-1; i++) {
			stack[i] = stack[i+1];
		}
		currentIndex--;
		return num;
	}
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < currentIndex; i++) {
			strBuilder = strBuilder.append(stack[i].toString() + ", ");
		}
		return strBuilder.toString();
	}
	
}

