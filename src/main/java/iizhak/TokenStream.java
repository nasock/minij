package iizhak;

public class TokenStream {
	private Token[] tokens;
	private int index;
	
	public TokenStream(Token[] tokens) {
		this.tokens = tokens;
		index = 0;
	}
	
	public Token next() {
		if (index >= tokens.length) {
			throw new IndexOutOfBoundsException();
		}
		int i = index;
		index++;
		return tokens[i];
	}
	
	public boolean hasNext() {
		return (index < tokens.length);
	}
	
	public int getIndex() {
		return index;
	}
	
	public Token[] getTokens() {
		return tokens;
	}
}
