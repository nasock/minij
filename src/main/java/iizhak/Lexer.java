package iizhak;

import java.util.*;

import iizhak.Token.TokenType;

public class Lexer {

	public Token[] tokenize(String programStr) {
		String[] words = programStr.split(" ");
		ArrayList<Token> tokenList = new ArrayList<Token>();
		int i = 0;
		while (i < words.length) {
			String data = words[i].trim();
			if (data.length() == 0) {
				i++;
				continue;
			}
			TokenType type;

			if (data.matches("[0-9]+")) {
				type = Token.TokenType.NUMBER;
			} else if (data.equals(":")) {
				type = Token.TokenType.COLON;
			} else if (data.equals(";")) {
				type = Token.TokenType.SEMICOLON;
			} else if (data.equals("begin")) {
				type = Token.TokenType.BEGIN;
			} else if (data.equals("while")) {
				type = Token.TokenType.WHILE;
			} else if (data.equals("if")) {
				type = Token.TokenType.IF;
			} else if (data.equals("else")) {
				type = Token.TokenType.ELSE;
			} else if (data.equals("end")) {
				type = Token.TokenType.END;
			} else {
				type = Token.TokenType.CALL;
			}

			tokenList.add(new Token(type, data));
			i++;
		}
		Token[] tokens = new Token[1];
		tokens = tokenList.toArray(tokens);

		return tokens;
	}
}

class InvalidSymbolException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 186627817188972650L;

}
