package iizhak;

public class Library {
	private Table table;
	
	public Library() {
		table = new Table();
		table.put("+", new Add());
		table.put("-", new Sub());
		table.put("*", new Mul());
		table.put("/", new Div());
		table.put(">", new GraterThen());
		table.put("<", new LessThen());
		table.put("=", new Equal());
		table.put(".", new PrintWord());
		table.put("roll", new Roll());
		table.put("pop", new Pop());
		table.put("dup", new Dup());
	}
	
	public int getIndex(String str) {
		return table.getIndex(str);
	}
	
	public int put(String str, FWord word) {
		return table.put(str, word);
	}
	
	public int set(String str, FWord word) {
		return table.set(str, word);
	}
	
	public FWord getWord(int i) {
		return table.getWord(i);
	}
}

class Add extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		int value = a.toInt() + b.toInt();
		stack.push(new FNum(value));
	}
	
	public String toString() {
		return "add";
	}
}

class Sub extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		int value = b.toInt() - a.toInt();
		stack.push(new FNum(value));
	}
	
	public String toString() {
		return "sub";
	}
}

class Mul extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		int value = a.toInt() * b.toInt();
		stack.push(new FNum(value));
	}
	
	public String toString() {
		return "mul";
	}
}

class Div extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		int value = a.toInt() / b.toInt();
		stack.push(new FNum(value));
	}
	
	public String toString() {
		return "div";
	}
}

class GraterThen extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		boolean value = (b.toInt() > a.toInt());
		int answer;
		if (value == true) {
			answer = 1;
		} else {
			answer = 0;
		}
		stack.push(new FNum(answer));
	}
	
	public String toString() {
		return "grater then";
	}
}

class LessThen extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		boolean value = (b.toInt() < a.toInt());
		int answer;
		if (value == true) {
			answer = 1;
		} else {
			answer = 0;
		}
		stack.push(new FNum(answer));
	}
	
	public String toString() {
		return "less then";
	}
}

class Equal extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.pop();
		FObject b = stack.pop();
		boolean value = (a.toInt() == b.toInt());
		int answer;
		if (value == true) {
			answer = 1;
		} else {
			answer = 0;
		}
		stack.push(new FNum(answer));
	}
	
	public String toString() {
		return "equal";
	}
}

class PrintWord extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		System.out.print(stack.top().toInt() + "\n");
	}

	public String toString() {
		return "print";
	}
}

class Roll extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject last = stack.pop();
		FObject prev = stack.pop();
		stack.push(last);
		stack.push(prev);
	}

	public String toString() {
		return "roll";
	}
}

class Pop extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		stack.pop();
	}

	public String toString() {
		return "pop";
	}
}

class Dup extends FNativeWord {

	public void call(State state) {
		Stack stack = state.getStack();
		FObject a = stack.top();
		stack.push(a);
	}

	public String toString() {
		return "dup";
	}
}

