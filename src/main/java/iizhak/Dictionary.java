package iizhak;

class Pair<K, V> {
	private K key;
	private V value;
	private Pair<K, V> next;

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
		this.next = null;
	}

	public Pair(K key, V value, Pair<K, V> next) {
		this.key = key;
		this.value = value;
		this.next = next;
	}

	public void setKey(K k) {
		key = k;
	}

	public void setValue(V v) {
		value = v;
	}

	public void setNext(Pair<K, V> n) {
		next = n;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public Pair<K, V> getNext() {
		return next;
	}

	public String toString() {
		return (key + ": " + value);
	}
}


public class Dictionary<K, V> {

	private Pair<K, V>[] array;
	private int size;
	final private double LOAD_FACTOR = 1.5;
	final private int CAPACITY = 1;

	@SuppressWarnings("unchecked")
	public Dictionary() {
		// array = (Pair <K, V>[]) new Object[3];
		array = new Pair[CAPACITY];
		size = 0;
	}

	public boolean isOverloaded() {
		return (size + 1) / array.length > LOAD_FACTOR;
	}

	public void put(K key, V value) {
		if (key == null) {
			throw new NullPointerException();
		}
		
		if (value == null) {
			throw new NullPointerException();
		}
		
		
		if (isOverloaded()) {
			resize();
		}

		int index = key.hashCode() % array.length;

		Pair<K, V> currentPair = array[index];
		Pair<K, V> newPair = new Pair<K, V>(key, value);

		if (currentPair == null) {
			array[index] = newPair;
		} else {

			K currentKey = currentPair.getKey();
			if (currentKey.equals(key)) {
				currentPair.setValue(value);
				return;
			}
			while (currentPair.getNext() != null) {
				currentPair = currentPair.getNext();
				currentKey = currentPair.getKey();
				if (currentKey.equals(key)) {
					currentPair.setValue(value);
					return;
				}
			}

			currentPair.setNext(newPair);
		}
		size++;
	}

	public V get(K key) {
		if (key == null) {
			throw new NullPointerException();
		}
		
		for (int i = 0; i < array.length; i++) {
			Pair<K, V> currentPair = array[i];

			if (currentPair != null) {
				K currentKey = currentPair.getKey();
				if (currentKey.equals(key)) {
					return currentPair.getValue();
				}
				while (currentPair.getNext() != null) {
					currentPair = currentPair.getNext();
					currentKey = currentPair.getKey();
					if (currentKey.equals(key)) {
						return currentPair.getValue();
					}
				}
			}
		}

		return null;
	}

	public V delete(K key) {
		if (key == null) {
			throw new NullPointerException();
		}
		
		Pair<K, V> pair = null;
		Pair<K, V> previousPair = null;
		int index = -1;
		boolean find = false;
		for (int i = 0; i < array.length; i++) {
			Pair<K, V> currentPair = array[i];
			if (currentPair != null) {
				K currentKey = currentPair.getKey();
				if (currentKey.equals(key)) {
					pair = currentPair;
					index = i;
					break;
				}
				while (currentPair.getNext() != null) {
					previousPair = currentPair;
					currentPair = currentPair.getNext();
					currentKey = currentPair.getKey();
					if (currentKey.equals(key)) {
						pair = currentPair;
						find = true;
						break;
					}
				}
				if (find) {
					break;
				}
			}
		}

		if (pair == null) {
			return null;
		} else if (previousPair == null) {
			array[index] = pair.getNext();
		} else {
			previousPair.setNext(pair.getNext());
		}

		size--;
		return pair.getValue();
	}

	@SuppressWarnings("unchecked")
	private void resize() {
		Pair<K, V>[] newArray = new Pair[array.length * 2];
		Pair<K, V>[] copyArray = array;
		array = newArray;
		size = 0;

		for (int i = 0; i < copyArray.length; i++) {
			Pair<K, V> currentPair = copyArray[i];
			if (currentPair != null) {
				K key = currentPair.getKey();
				V value = currentPair.getValue();
				put(key, value);
				while (currentPair.getNext() != null) {
					currentPair = currentPair.getNext();
					key = currentPair.getKey();
					value = currentPair.getValue();
					put(key, value);
				}
			}
		}
	}

	public String toString() {
		StringBuilder strb = new StringBuilder("{");
		for (int i = 0; i < array.length; i++) {
			Pair<K, V> currentPair = array[i];
			if (currentPair != null) {
				strb.append(currentPair.toString() + "; ");
				while (currentPair.getNext() != null) {
					currentPair = currentPair.getNext();
					strb.append(currentPair.toString() + "; ");
				}
			}
		}
		strb.append("}");
		return strb.toString();
	}

	public int size() {
		return size;
	}

	public static void main(String[] args) {
		Dictionary<String, Integer> dic = new Dictionary<String, Integer>();

		dic.put("s1", 1);
		dic.put("s2", 2);
		dic.put("s3", 3);
		dic.put("s4", 4);
		dic.put("s5", 5);
		dic.put("s6", 6);
		dic.put("s3", 333);
		dic.put("s7", 7);
		dic.put("s8", 8);
		dic.put("s9", 9);

		System.out.println("\n" + dic + " " + dic.size());
		Integer answer0 = dic.get("s3");
		Integer answer1 = dic.get("555");
		System.out.println(answer0 + ", " + answer1);

		Integer answer2 = dic.delete("s3");
		Integer answer3 = dic.delete("555");
		System.out.println(answer2 + ", " + answer3);
		System.out.println(dic + " " + dic.size());
	}
}
