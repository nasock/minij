package calculator;

public abstract class Value {

	public abstract Value add(Value value2);

	public abstract Value sub(Value value2);

	public abstract Value mul(Value value2);

	public abstract Value div(Value value2);

	public abstract Value mod(Value value2);

	public abstract Value eq(Value value2);

	public abstract Value ne(Value value2);

	public abstract Value ge(Value value2);

	public abstract Value gt(Value value2);

	public abstract Value se(Value value2);

	public abstract Value st(Value value2);

	public abstract int toInt();

	public abstract float toFloat();

	public abstract boolean toBoolean();

}
