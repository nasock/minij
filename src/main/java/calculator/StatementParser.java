package calculator;

import java.util.ArrayList;

import calculator.expression.BlockExpression;
import calculator.expression.Expression;
import calculator.expression.ForExpression;
import calculator.expression.IfExpression;
import calculator.expression.WhileExpression;

public class StatementParser {
	private ExpressionParser expressionParser;

	public StatementParser() {
		this.expressionParser = new ExpressionParser();
	}

	public ArrayList<Expression> parseStatements(Lexer lexer) {
		ArrayList<Expression> expressions = new ArrayList<Expression>();
		while (!lexer.isFinished()) {
			Expression expression = parseStatement(lexer);
			expressions.add(expression);
		}
		return expressions;
	}

	public Expression parseStatement(Lexer lexer) {
		Expression expression;
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.IF) {
			lexer.advance();
			expression = parseIf(lexer);
		} else if (type == Token.Type.WHILE) {
			lexer.advance();
			expression = parseWhile(lexer);
		} else if (type == Token.Type.FOR) {
			lexer.advance();
			expression = parseFor(lexer);
		} else if (type == Token.Type.LBRACKET) {
			lexer.advance();
			expression = parseBlock(lexer);
		} else {
			expression = expressionParser.parse(lexer);
			lexer.checkTokenTypeAndAdvance(Token.Type.SEMI);
		}
		return expression;
	}

	private Expression parseFor(Lexer lexer) {
		lexer.checkTokenTypeAndAdvance(Token.Type.LPAREN);
		BlockExpression initializeExpression = parseForBlock(lexer);
		Expression conditionExpression = parseStatement(lexer);
		BlockExpression incrementExpression = parseForBlock(lexer);
		Expression bodyExpression = parseStatement(lexer);
		return new ForExpression(initializeExpression, conditionExpression, incrementExpression, bodyExpression);
	}

	private BlockExpression parseForBlock(Lexer lexer) {
		Token.Type type = lexer.getCurrentTokenType();
		ArrayList<Expression> expressions = new ArrayList<Expression>();
		while (true) {
			Expression expression = expressionParser.parse(lexer);
			expressions.add(expression);
			type = lexer.getCurrentTokenType();
			if (type != Token.Type.COMA) {
				if (type == Token.Type.SEMI || type == Token.Type.RPAREN) {
					break;
				} else {
					throw new SyntaxException("coma expected");
				}
			} else {
				lexer.advance();
				type = lexer.getCurrentTokenType();
				if (type == Token.Type.SEMI) {
					throw new SyntaxException("coma is invalid");
				}
			}
		}

		lexer.advance();
		return new BlockExpression(expressions);
	}

	private Expression parseIf(Lexer lexer) {
		lexer.checkTokenTypeAndAdvance(Token.Type.LPAREN);
		Expression conditionExpression = expressionParser.parse(lexer);
		lexer.checkTokenTypeAndAdvance(Token.Type.RPAREN);
		Expression trueExpression = parseStatement(lexer);
		Expression falseExpression = null;
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.ELSE) {
			lexer.advance();
			falseExpression = parseStatement(lexer);
		}
		return new IfExpression(conditionExpression, trueExpression, falseExpression);
	}

	private Expression parseWhile(Lexer lexer) {
		lexer.checkTokenTypeAndAdvance(Token.Type.LPAREN);
		Expression conditionExpression = expressionParser.parse(lexer);
		lexer.checkTokenTypeAndAdvance(Token.Type.RPAREN);
		Expression bodyExpression = parseStatement(lexer);
		return new WhileExpression(conditionExpression, bodyExpression);
	}

	private Expression parseBlock(Lexer lexer) {
		ArrayList<Expression> expressions = new ArrayList<Expression>();
		Token.Type type = lexer.getCurrentTokenType();
		while (type != Token.Type.RBRACKET) {
			Expression expr = parseStatement(lexer);
			expressions.add(expr);
			type = lexer.getCurrentTokenType();
		}
		lexer.advance();
		return new BlockExpression(expressions);
	}

}
