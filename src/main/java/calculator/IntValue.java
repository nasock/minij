package calculator;

public class IntValue extends Value {
	int value;

	public IntValue(int value) {
		this.value = value;
	}

	@Override
	public Value add(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new IntValue(value + valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue((float) value + valueFloat.value);
		} else {
			throw new SyntaxException("invalid type: int expected");
		}
	}

	@Override
	public Value sub(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new IntValue(value - valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue((float) value - valueFloat.value);
		} else {
			throw new SyntaxException("invalid type: int expected");
		}
	}

	@Override
	public Value mul(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new IntValue(value * valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue((float) value * valueFloat.value);
		} else {
			throw new SyntaxException("invalid type: int expected");
		}
	}

	@Override
	public Value div(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			if (value % valueInt.value == 0) {
				return new IntValue(value / valueInt.value);
			} else {
				return new FloatValue((float) value / (float) valueInt.value);
			}
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue((float) value / valueFloat.value);
		} else {
			throw new SyntaxException("invalid type: int expected");
		}
	}

	@Override
	public Value mod(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new IntValue(value % valueInt.value);
		} else {
			throw new SyntaxException("invalid type: int expected");
		}
	}

	@Override
	public Value eq(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return trueValue;
		}
		if (!(value2 instanceof IntValue)) {
			return falseValue;
		}
		IntValue int2 = (IntValue) value2;
		if (value == (int2.value)) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value ne(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return falseValue;
		}
		if (!(value2 instanceof IntValue)) {
			return trueValue;
		}
		IntValue int2 = (IntValue) value2;
		if (value == (int2.value)) {
			return falseValue;
		}
		return trueValue;
	}

	@Override
	public Value ge(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof FloatValue) {
			FloatValue float2 = (FloatValue) value2;
			Float otherValue = float2.value;
			if (new Float(value) >= otherValue) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof IntValue)) {
			throw new UnsupportedOperationException();
		}
		IntValue int2 = (IntValue) value2;
		if (value >= int2.value) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value gt(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof FloatValue) {
			FloatValue float2 = (FloatValue) value2;
			Float otherValue = float2.value;
			if (new Float(value) > otherValue) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof IntValue)) {
			throw new UnsupportedOperationException();
		}
		IntValue int2 = (IntValue) value2;
		if (value > int2.value) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value se(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof FloatValue) {
			FloatValue float2 = (FloatValue) value2;
			Float otherValue = float2.value;
			if (new Float(value) <= otherValue) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof IntValue)) {
			throw new UnsupportedOperationException();
		}
		IntValue int2 = (IntValue) value2;
		if (value <= int2.value) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value st(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof FloatValue) {
			FloatValue float2 = (FloatValue) value2;
			Float otherValue = float2.value;
			if (new Float(value) < otherValue) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof IntValue)) {
			throw new UnsupportedOperationException();
		}
		IntValue int2 = (IntValue) value2;
		if (value < int2.value) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public int toInt() {
		return value;
	}

	@Override
	public float toFloat() {
		return value;
	}

	@Override
	public boolean toBoolean() {
		return value != 0;
	}

	@Override
	public int hashCode() {
		return value;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof IntValue)) {
			return false;
		}
		IntValue otherInt = (IntValue) other;
		return value == (otherInt.value);
	}

	@Override
	public String toString() {
		return "Int Value: " + value;
	}
}
