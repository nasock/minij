package calculator;

import java.util.ArrayList;
import java.util.HashMap;

import calculator.expression.*;

public class Interpreter {
	private StatementParser sParser;
	private HashMap<Value, Value> variables;

	public Interpreter() {
		sParser = new StatementParser();
		this.variables = new HashMap<Value, Value>();
	}

	public void setVariable(Value name, Value value) {
		variables.put(name, value);
	}

	public Value getVariable(Value name) {
		return variables.get(name);
	}

	public Value calculate(String input) {
		Lexer lexer = new Lexer(input);
		ArrayList<Expression> expressions = sParser.parseStatements(lexer);
		return calculate(expressions);
	}

	private Value calculate(ArrayList<Expression> expressions) {
		Value result = null;
		for (Expression expression : expressions) {
			result = expression.eval(this);
		}
		return result;
	}
}
