package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class AddExpression extends BinaryExpression {

	public AddExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.add(valueRight);
	}

	@Override
	public String toString() {
		return "Add Expression: " + "left: " + left + "right: " + right + "\n";
	}
}
