package calculator.expression;

import calculator.FalseValue;
import calculator.Interpreter;
import calculator.TrueValue;
import calculator.UnsupportedOperationException;
import calculator.Value;

public class ForExpression extends Expression {
	private Expression initializeExpression;
	private Expression conditionExpression;
	private Expression incrementExpression;
	private Expression bodyExpression;

	public ForExpression(Expression initializeExpression, Expression conditionExpression,
			Expression incrementExpression, Expression bodyExpression) {
		this.initializeExpression = initializeExpression;
		this.conditionExpression = conditionExpression;
		this.incrementExpression = incrementExpression;
		this.bodyExpression = bodyExpression;
	}

	@Override
	public Value eval(Interpreter calculator) {
		initializeExpression.eval(calculator);
		Value bool = conditionExpression.eval(calculator);
		while (true) {
			if (bool == TrueValue.getInstance()) {
				bodyExpression.eval(calculator);
				incrementExpression.eval(calculator);
			} else if (bool == FalseValue.getInstance()) {
				break;
			} else {
				throw new UnsupportedOperationException();
			}
			bool = conditionExpression.eval(calculator);
		}
		return null;
	}

	@Override
	public String toString() {
		return "For Expression: " + "init: " + initializeExpression + "cond: " + conditionExpression + "incr: "
				+ incrementExpression + "body: " + bodyExpression + "\n";
	}

}
