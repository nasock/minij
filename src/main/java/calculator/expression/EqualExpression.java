package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class EqualExpression extends BinaryExpression {

	public EqualExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.eq(valueRight);
	}

	@Override
	public String toString() {
		return "Equal Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
