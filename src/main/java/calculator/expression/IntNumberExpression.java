package calculator.expression;

import calculator.Interpreter;
import calculator.IntValue;
import calculator.Value;

public class IntNumberExpression extends Expression {
	private int value;

	public IntNumberExpression(int value) {
		this.value = value;
	}

	@Override
	public Value eval(Interpreter calculator) {
		return new IntValue(value);
	}

	@Override
	public String toString() {
		return "Int Expression: " + value + "\n";
	}
}
