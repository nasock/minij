package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class AssignExpression extends BinaryExpression {

	public AssignExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		calculator.setVariable(valueLeft, valueRight);
		return valueRight;
	}

	@Override
	public String toString() {
		return "Assign Expression: " + "left: " + left + "right: " + right + "\n";
	}
}
