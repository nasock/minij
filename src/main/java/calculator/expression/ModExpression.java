package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class ModExpression extends BinaryExpression {

	public ModExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.mod(valueRight);
	}
	
	@Override
	public String toString() {
		return "Mod Expression: " + "left: " + left + "right: " + right  + "\n";
	}

}
