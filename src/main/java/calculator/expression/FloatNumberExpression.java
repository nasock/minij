package calculator.expression;

import calculator.*;

public class FloatNumberExpression extends Expression {
	private float value;

	public FloatNumberExpression(float value) {
		this.value = value;
	}

	@Override
	public Value eval(Interpreter calculator) {
		return new FloatValue(value);
	}

	@Override
	public String toString() {
		return "Float Expression: " + value + "\n";
	}
}
