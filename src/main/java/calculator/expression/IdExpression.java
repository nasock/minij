package calculator.expression;

import calculator.Interpreter;
import calculator.SymbolValue;
import calculator.Value;

public class IdExpression extends Expression {
	private SymbolValue value;

	public IdExpression(SymbolValue value) {
		this.value = value;
	}

	@Override
	public Value eval(Interpreter calculator) {
		return value;
	}

	@Override
	public String toString() {
		return "Id Expression: " + value + "\n";
	}
}
