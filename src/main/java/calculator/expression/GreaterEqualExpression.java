package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class GreaterEqualExpression extends BinaryExpression {

	public GreaterEqualExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.ge(valueRight);
	}

	@Override
	public String toString() {
		return "GE Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
