package calculator.expression;

import java.util.ArrayList;

import calculator.Interpreter;
import calculator.Value;

public class BlockExpression extends Expression {
	private ArrayList<Expression> expressions;

	public BlockExpression(ArrayList<Expression> expressions) {
		this.expressions = expressions;
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value result = null;
		for (Expression expr : expressions) {
			result = expr.eval(calculator);
		}
		return result;
	}

	@Override
	public String toString() {
		String str = "Block Expression: ";
		for (Expression expression : expressions) {
			str = str + expression;
		}
		return str;
	}

}
