package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class GreaterThenExpression extends BinaryExpression {

	public GreaterThenExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.gt(valueRight);
	}
	
	@Override
	public String toString() {
		return "GT Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
