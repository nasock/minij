package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class NotEqualExpression extends BinaryExpression {

	public NotEqualExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.ne(valueRight);
	}
	
	@Override
	public String toString() {
		return "NE Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
