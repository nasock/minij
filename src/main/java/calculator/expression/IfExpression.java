package calculator.expression;

import calculator.Interpreter;
import calculator.FalseValue;
import calculator.TrueValue;
import calculator.UnsupportedOperationException;
import calculator.Value;

public class IfExpression extends Expression {
	private Expression conditionExpression;
	private Expression trueExpression;
	private Expression falseExpression;

	public IfExpression(Expression conditionExpression, Expression trueExpression, Expression falseExpression) {
		this.conditionExpression = conditionExpression;
		this.trueExpression = trueExpression;
		this.falseExpression = falseExpression;
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value bool = conditionExpression.eval(calculator);
		if (bool == TrueValue.getInstance()) {
			trueExpression.eval(calculator);
		} else if (bool == FalseValue.getInstance()) {
			if (falseExpression != null) {
				falseExpression.eval(calculator);
			}
		} else {
			throw new UnsupportedOperationException();
		}
		return null;
	}

	@Override
	public String toString() {
		return "If Expression: " + "cond: " + conditionExpression + "true: " + trueExpression + "false"
				+ falseExpression + "\n";
	}

}
