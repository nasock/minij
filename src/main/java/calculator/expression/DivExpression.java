package calculator.expression;

import calculator.Interpreter;
import calculator.SymbolValue;
import calculator.SyntaxException;
import calculator.Value;

public class DivExpression extends BinaryExpression {

	public DivExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		if (valueLeft instanceof SymbolValue) {
			valueLeft = calculator.getVariable(valueLeft);
			if (valueLeft == null) {
				throw new SyntaxException("undefined variable");
			}
		}
		Value valueRight = right.eval(calculator);
		return valueLeft.div(valueRight);
	}

	@Override
	public String toString() {
		return "Div Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
