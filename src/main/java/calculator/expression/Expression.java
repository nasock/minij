package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public abstract class Expression {
	public abstract Value eval(Interpreter calculator);
}
