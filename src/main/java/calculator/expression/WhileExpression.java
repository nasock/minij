package calculator.expression;

import calculator.Interpreter;
import calculator.FalseValue;
import calculator.TrueValue;
import calculator.UnsupportedOperationException;
import calculator.Value;

public class WhileExpression extends Expression {
	private Expression conditionExpression;
	private Expression bodyExpression;

	public WhileExpression(Expression conditionExpression, Expression bodyExpression) {
		this.conditionExpression = conditionExpression;
		this.bodyExpression = bodyExpression;
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value bool = conditionExpression.eval(calculator);
		while (true) {
			if (bool == TrueValue.getInstance()) {
				bodyExpression.eval(calculator);
			} else if (bool == FalseValue.getInstance()) {
				break;
			} else {
				throw new UnsupportedOperationException();
			}
			bool = conditionExpression.eval(calculator);
		}
		return null;
	}

	@Override
	public String toString() {
		return "While Expression: " + "cond: " + conditionExpression + "body: " + bodyExpression + "\n";
	}

}
