package calculator.expression;

import calculator.Interpreter;
import calculator.SymbolValue;
import calculator.SyntaxException;
import calculator.Value;

public class VarExpression extends Expression {
	private SymbolValue value;

	public VarExpression(String value) {
		this.value = new SymbolValue(value);
	}

	public SymbolValue getName() {
		return value;
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value evalValue = calculator.getVariable(value);
		if (evalValue == null) {
			throw new SyntaxException("undefined variable");
		}
		return evalValue;
	}

	@Override
	public String toString() {
		return "Add Expression: " + value;
	}

}
