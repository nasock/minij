package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class SubExpression extends BinaryExpression {

	public SubExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.sub(valueRight);
	}
	
	@Override
	public String toString() {
		return "Sub Expression" + "left: " + left + "right: " + right + "\n";
	}
}
