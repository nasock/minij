package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class SmallerEqualExpression extends BinaryExpression {

	public SmallerEqualExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.se(valueRight);
	}

	@Override
	public String toString() {
		return "SE Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
