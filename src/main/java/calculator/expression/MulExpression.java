package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class MulExpression extends BinaryExpression {

	public MulExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.mul(valueRight);
	}

	@Override
	public String toString() {
		return "Mul Expression: " + "left: " + left + "right: " + right + "\n";
	}

}
