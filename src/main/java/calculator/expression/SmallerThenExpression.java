package calculator.expression;

import calculator.Interpreter;
import calculator.Value;

public class SmallerThenExpression extends BinaryExpression {

	public SmallerThenExpression(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public Value eval(Interpreter calculator) {
		Value valueLeft = left.eval(calculator);
		Value valueRight = right.eval(calculator);
		return valueLeft.st(valueRight);
	}

	@Override
	public String toString() {
		return "ST Expression" + "left: " + left + "right: " + right + "\n";
	}

}
