package calculator;

public class Token {
	enum Type {
		INTNUM, FLOATNUM, ID, 
		ADD, SUB, MUL, DIV, MOD, ASSIGN, 
		EQUAL, NOTEQUAL, GREATEREQUAL, SMALLEREQUAL, GREATERTHEN, SMALLERTHEN, 
		IF, ELSE, WHILE, FOR, SWITCH, CLASS, 
		LBRACKET, RBRACKET, LPAREN, RPAREN, SEMI, COMA;
	}

	private String data;
	private Type type;
	private int line;
	private int collumn;

	public Token(String data, Type type, int line, int collumn) {
		this.data = data;
		this.type = type;
		this.line = line;
		this.collumn = collumn;
	}

	public Token(char ch, Type type, int line, int collumn) {
		this("" + ch, type, line, collumn);
	}

	public String getData() {
		return data;
	}

	public Type getType() {
		return type;
	}

	public int getLine() {
		return line;
	}

	public int getCollumn() {
		return collumn;
	}

	@Override
	public String toString() {
		return type.toString() + ": " + data + ", " + line + ", " + collumn;
	}

}
