package calculator;

public class Api {

	public static Value createBoolean(boolean b) {
		if (b) {
			return TrueValue.getInstance();
		} else {
			return FalseValue.getInstance();
		}
	}

	public static boolean isTrue(Value value) {
		if (value == TrueValue.getInstance()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isFalse(Value value) {
		if (value == FalseValue.getInstance()) {
			return true;
		} else {
			return false;
		}
	}

}
