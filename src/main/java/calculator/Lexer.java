package calculator;

import java.util.HashMap;

public class Lexer {
	private String input;
	private int currentIndex;
	private int currentLine;
	private int currentCollumn;
	private Token currentToken;
	private HashMap<String, Token.Type> keyWords;

	public Lexer(String input) {
		this.input = input;
		currentIndex = 0;
		currentLine = 0;
		currentCollumn = 0;
		makeKeyWords();
		checkWhitespace();
		makeToken();
	}

	private void makeKeyWords() {
		keyWords = new HashMap<String, Token.Type>();
		keyWords.put("if", Token.Type.IF);
		keyWords.put("else", Token.Type.ELSE);
		keyWords.put("while", Token.Type.WHILE);
		keyWords.put("for", Token.Type.FOR);
		keyWords.put("switch", Token.Type.SWITCH);
		keyWords.put("class", Token.Type.CLASS);
	}

	public Token getCurrentToken() {
		return currentToken;
	}

	public Token.Type getCurrentTokenType() {
		return currentToken.getType();
	}

	public boolean isFinished() {
		return currentIndex >= input.length();
	}

	private void advanceIndex() {
		currentIndex++;
		currentCollumn++;
	}

	public void advance() {
		advanceIndex();
		checkWhitespace();
		makeToken();
	}

	public void checkTokenTypeAndAdvance(Token.Type type) {
		if (currentToken == null) {
			throw new SyntaxException("no token of type " + type);
		} else if (currentToken.getType() != type) {
			throw new SyntaxException("invalid type", currentToken);
		}
		advance();
	}

	private void checkWhitespace() {
		while (!isFinished()) {
			char ch = input.charAt(currentIndex);
			if (ch == '\n') {
				currentIndex++;
				currentLine++;
				currentCollumn = 0;
			} else if (ch == ' ') {
				advanceIndex();
			} else {
				break;
			}
		}
	}

	private void makeToken() {
		if (isFinished()) {
			currentToken = null;
			return;
		}
		char ch = input.charAt(currentIndex);
		Token token;
		if (ch == '+') {
			token = new Token(ch, Token.Type.ADD, currentLine, currentCollumn);
		} else if (ch == '-') {
			token = new Token(ch, Token.Type.SUB, currentLine, currentCollumn);
		} else if (ch == '*') {
			token = new Token(ch, Token.Type.MUL, currentLine, currentCollumn);
		} else if (ch == '/') {
			token = new Token(ch, Token.Type.DIV, currentLine, currentCollumn);
		} else if (ch == '%') {
			token = new Token(ch, Token.Type.MOD, currentLine, currentCollumn);
		} else if (Character.isDigit(ch)) {
			token = makeNum(ch);
		} else if (Character.isAlphabetic(ch) || ch == '_') {
			token = makeId(ch);
		} else if (ch == '=') {
			token = makeEqual(ch);
		} else if (ch == '!') {
			token = makeNotEqual(ch);
		} else if (ch == '>') {
			token = makeGreater(ch);
		} else if (ch == '<') {
			token = makeSmaller(ch);
		} else if (ch == '(') {
			token = new Token(ch, Token.Type.LPAREN, currentLine, currentCollumn);
		} else if (ch == ')') {
			token = new Token(ch, Token.Type.RPAREN, currentLine, currentCollumn);
		} else if (ch == '{') {
			token = new Token(ch, Token.Type.LBRACKET, currentLine, currentCollumn);
		} else if (ch == '}') {
			token = new Token(ch, Token.Type.RBRACKET, currentLine, currentCollumn);
		} else if (ch == ';') {
			token = new Token(ch, Token.Type.SEMI, currentLine, currentCollumn);
		} else if (ch == ',') {
			token = new Token(ch, Token.Type.COMA, currentLine, currentCollumn);
		} else {
			throw new SyntaxException("invalid symbol");
		}
		currentToken = token;
	}

	private Token makeSmaller(char ch) {
		Token token;
		int i = currentIndex + 1;
		if (i < input.length()) {
			char currentCh = input.charAt(i);
			if (currentCh == '=') {
				token = new Token("<=", Token.Type.SMALLEREQUAL, currentLine, currentCollumn);
				currentIndex++;
				currentCollumn++;
				return token;
			}
		}
		return new Token(ch, Token.Type.SMALLERTHEN, currentLine, currentCollumn);
	}

	private Token makeGreater(char ch) {
		Token token;
		int i = currentIndex + 1;
		if (i < input.length()) {
			char currentCh = input.charAt(i);
			if (currentCh == '=') {
				token = new Token(">=", Token.Type.GREATEREQUAL, currentLine, currentCollumn);
				currentIndex++;
				currentCollumn++;
				return token;
			}
		}
		return new Token(ch, Token.Type.GREATERTHEN, currentLine, currentCollumn);
	}

	private Token makeNotEqual(char ch) {
		Token token;
		int i = currentIndex + 1;
		if (i < input.length()) {
			char currentCh = input.charAt(i);
			if (currentCh == '=') {
				token = new Token("!=", Token.Type.NOTEQUAL, currentLine, currentCollumn);
				currentIndex++;
				currentCollumn++;
				return token;
			}
		}
		throw new SyntaxException("invalid symbol");

	}

	private Token makeEqual(char ch) {
		Token token;
		int i = currentIndex + 1;
		if (i < input.length()) {
			char currentCh = input.charAt(i);
			if (currentCh == '=') {
				token = new Token("==", Token.Type.EQUAL, currentLine, currentCollumn);
				currentIndex++;
				currentCollumn++;
				return token;
			}
		}

		return new Token(ch, Token.Type.ASSIGN, currentLine, currentCollumn);
	}

	private Token makeId(char ch) {
		String data = "" + ch;
		int i = currentIndex + 1;
		for (; i < input.length(); i++) {
			ch = input.charAt(i);
			if (!(Character.isDigit(ch) || Character.isAlphabetic(ch) || ch == '_')) {
				break;
			}
			data = data + ch;
		}
		Token token;
		if (keyWords.keySet().contains(data)) {
			token = new Token(data, keyWords.get(data), currentLine, currentCollumn);
		} else {
			token = new Token(data, Token.Type.ID, currentLine, currentCollumn);
		}
		setNewIndexes(i);
		return token;
	}

	private Token makeNum(char ch) {
		String data = "" + ch;
		Token.Type type = Token.Type.INTNUM;
		int i = currentIndex + 1;
		for (; i < input.length(); i++) {
			ch = input.charAt(i);
			if (!Character.isDigit(ch)) {
				if (ch == '.') {
					if (type == Token.Type.INTNUM) {
						type = Token.Type.FLOATNUM;
					} else {
						throw new SyntaxException("invalid number");
					}
				} else {
					break;
				}
			}
			data = data + ch;
		}

		Token token = new Token(data, type, currentLine, currentCollumn);
		setNewIndexes(i);
		return token;
	}

	private void setNewIndexes(int i) {
		currentCollumn = currentCollumn + (i - 1 - currentIndex);
		currentIndex = i - 1;
	}

}
