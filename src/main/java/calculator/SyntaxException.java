package calculator;

public class SyntaxException extends RuntimeException {

	public SyntaxException(String string) {
		super(string);
	}

	public SyntaxException(String string, Token token) {
		super(string + ": " + token.toString());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -585849176119662655L;

}
