package calculator;

public class FloatValue extends Value {
	float value;

	public FloatValue(float value) {
		this.value = value;
	}

	@Override
	public Value add(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new FloatValue(value + (float) valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue(value + valueFloat.value);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public Value sub(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new FloatValue(value - (float) valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue(value - valueFloat.value);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public Value mul(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new FloatValue(value * (float) valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue(value * valueFloat.value);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public Value div(Value value2) {
		if (value2 instanceof IntValue) {
			IntValue valueInt = (IntValue) value2;
			return new FloatValue(value / (float) valueInt.value);
		} else if (value2 instanceof FloatValue) {
			FloatValue valueFloat = (FloatValue) value2;
			return new FloatValue(value / valueFloat.value);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public Value mod(Value value2) {
		throw new SyntaxException("invalid type: int expected");
	}

	@Override
	public Value eq(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return trueValue;
		}
		if (!(value2 instanceof FloatValue)) {
			return falseValue;
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if ((new Float(value)).equals(otherValue)) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value ne(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return falseValue;
		}
		if (!(value2 instanceof FloatValue)) {
			return trueValue;
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if ((new Float(value)).equals(otherValue)) {
			return falseValue;
		}
		return trueValue;
	}

	@Override
	public Value ge(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof IntValue) {
			IntValue int2 = (IntValue) value2;
			if ((new Float(value)) >= new Float(int2.value)) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof FloatValue)) {
			throw new UnsupportedOperationException();
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if (new Float(value) >= otherValue) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value gt(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof IntValue) {
			IntValue int2 = (IntValue) value2;
			if ((new Float(value)) > new Float(int2.value)) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof FloatValue)) {
			throw new UnsupportedOperationException();
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if (new Float(value) > otherValue) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value se(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof IntValue) {
			IntValue int2 = (IntValue) value2;
			if ((new Float(value)) <= new Float(int2.value)) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof FloatValue)) {
			throw new UnsupportedOperationException();
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if (new Float(value) <= otherValue) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value st(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (value2 instanceof IntValue) {
			IntValue int2 = (IntValue) value2;
			if ((new Float(value)) < new Float(int2.value)) {
				return trueValue;
			}
			return falseValue;
		} else if (!(value2 instanceof FloatValue)) {
			throw new UnsupportedOperationException();
		}
		FloatValue float2 = (FloatValue) value2;
		Float otherValue = float2.value;
		if (new Float(value) < otherValue) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public int toInt() {
		return (int) value;
	}

	@Override
	public float toFloat() {
		return value;
	}

	@Override
	public boolean toBoolean() {
		return value != 0;
	}

	@Override
	public int hashCode() {
		return (new Float(value)).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FloatValue)) {
			return false;
		}
		FloatValue otherFloat = (FloatValue) other;
		Float otherValue = otherFloat.value;
		return (new Float(value)).equals(otherValue);
	}

	@Override
	public String toString() {
		return "Float Value: " + value;
	}

}
