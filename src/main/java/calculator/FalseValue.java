package calculator;

public class FalseValue extends Value {
	private static FalseValue single_instance = null;

	private FalseValue() {
	}

	public static FalseValue getInstance() {
		if (single_instance == null) {
			single_instance = new FalseValue();
		}
		return single_instance;
	}

	@Override
	public Value add(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value sub(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value mul(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value div(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value mod(Value valueRight) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value eq(Value value2) {
		return Api.createBoolean(this == value2);
	}

	@Override
	public Value ne(Value value2) {
		return Api.createBoolean(this != value2);
	}

	@Override
	public Value ge(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value gt(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value se(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value st(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int toInt() {
		return 0;
	}

	@Override
	public float toFloat() {
		return 0;
	}

	@Override
	public boolean toBoolean() {
		return false;
	}

	@Override
	public int hashCode() {
		return (new Boolean(false)).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "False Value";
	}

}
