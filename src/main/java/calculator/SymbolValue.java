package calculator;

public class SymbolValue extends Value {
	private String value;

	public SymbolValue(String value) {
		this.value = value;
	}

	public String getName() {
		return value;
	}

	@Override
	public Value add(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value sub(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value mul(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value div(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value mod(Value valueRight) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value eq(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return trueValue;
		}
		if (!(value2 instanceof SymbolValue)) {
			return falseValue;
		}
		SymbolValue symbol2 = (SymbolValue) value2;
		if (value.equals(symbol2.value)) {
			return trueValue;
		}
		return falseValue;
	}

	@Override
	public Value ne(Value value2) {
		Value trueValue = TrueValue.getInstance();
		Value falseValue = FalseValue.getInstance();
		if (this == value2) {
			return falseValue;
		}
		if (!(value2 instanceof IntValue)) {
			return trueValue;
		}
		SymbolValue symbol2 = (SymbolValue) value2;
		if (value.equals(symbol2.value)) {
			return falseValue;
		}
		return trueValue;
	}

	@Override
	public Value ge(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value gt(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value se(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Value st(Value value2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int toInt() {
		throw new UnsupportedOperationException();
	}

	@Override
	public float toFloat() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean toBoolean() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SymbolValue)) {
			return false;
		}
		SymbolValue otherString = (SymbolValue) other;
		return value.equals(otherString.value);
	}

	@Override
	public String toString() {
		return "String Value: " + value;
	}

}
