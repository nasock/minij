package calculator;

import calculator.expression.*;

public class ExpressionParser {

	public Expression parse(Lexer lexer) {
		Expression expression = parseAssign(lexer);
		return expression;
	}

	// public ArrayList<Expression> parseStatements(Lexer lexer) {
	// ArrayList<Expression> expressions = new ArrayList<Expression>();
	// while (!lexer.isFinished()) {
	// Expression expression = parseAssign(lexer);
	// expressions.add(expression);
	// }
	// return expressions;
	// }

	private Expression parseAssign(Lexer lexer) {
		Expression operand1 = parseBoolean(lexer);
		if (lexer.isFinished()) {
			return operand1;
		}
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.ASSIGN) {
			if (!(operand1 instanceof VarExpression)) {
				throw new SyntaxException("id expected");
			}
			VarExpression varExp = (VarExpression) operand1;
			operand1 = new IdExpression(varExp.getName());
			lexer.advance();
			Expression operand2 = parseBoolean(lexer);
			return new AssignExpression(operand1, operand2);
		} else {
			return operand1;
		}
	}

	private Expression parseBoolean(Lexer lexer) {
		Expression operand1 = parsePluse(lexer);
		if (lexer.isFinished()) {
			return operand1;
		}
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.EQUAL) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new EqualExpression(operand1, operand2);
		} else if (type == Token.Type.NOTEQUAL) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new NotEqualExpression(operand1, operand2);
		} else if (type == Token.Type.GREATEREQUAL) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new GreaterEqualExpression(operand1, operand2);
		} else if (type == Token.Type.GREATERTHEN) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new GreaterThenExpression(operand1, operand2);
		} else if (type == Token.Type.SMALLEREQUAL) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new SmallerEqualExpression(operand1, operand2);
		} else if (type == Token.Type.SMALLERTHEN) {
			lexer.advance();
			Expression operand2 = parsePluse(lexer);
			return new SmallerThenExpression(operand1, operand2);
		} else {
			return operand1;
		}
	}

	private Expression parsePluse(Lexer lexer) {
		Expression operand1 = parseMult(lexer);
		if (lexer.isFinished()) {
			return operand1;
		}
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.ADD) {
			lexer.advance();
			Expression operand2 = parseMult(lexer);
			return new AddExpression(operand1, operand2);
		} else if (type == Token.Type.SUB) {
			lexer.advance();
			Expression operand2 = parseMult(lexer);
			return new SubExpression(operand1, operand2);
		} else {
			return operand1;
		}
	}

	private Expression parseMult(Lexer lexer) {
		Expression operand1 = parseUnary(lexer);
		if (lexer.isFinished()) {
			return operand1;
		}
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.MUL) {
			lexer.advance();
			Expression operand2 = parseUnary(lexer);
			return new MulExpression(operand1, operand2);
		} else if (type == Token.Type.DIV) {
			lexer.advance();
			Expression operand2 = parseUnary(lexer);
			return new DivExpression(operand1, operand2);
		} else if (type == Token.Type.MOD) {
			lexer.advance();
			Expression operand2 = parseUnary(lexer);
			return new ModExpression(operand1, operand2);
		} else {
			return operand1;
		}
	}

	private Expression parseUnary(Lexer lexer) {
		if (lexer.isFinished()) {
			return null;
		}
		Expression expression;
		Token.Type type = lexer.getCurrentTokenType();
		if (type == Token.Type.LPAREN) {
			lexer.advance();
			expression = parse(lexer);
			lexer.checkTokenTypeAndAdvance(Token.Type.RPAREN);
			return expression;
		} else if (type == Token.Type.ID) {
			String value = lexer.getCurrentToken().getData();
			expression = new VarExpression(value);
		} else {
			Token token = lexer.getCurrentToken();
			if (type == Token.Type.INTNUM) {
				int value = Integer.parseInt(token.getData());
				expression = new IntNumberExpression(value);
			} else if (type == Token.Type.FLOATNUM) {
				float value = Float.parseFloat(token.getData());
				expression = new FloatNumberExpression(value);
			} else {
				throw new SyntaxException("invalid token", token);
			}
		}
		lexer.advance();
		return expression;
	}

}
